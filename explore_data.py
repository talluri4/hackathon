


import os
import glob
import random

import tensorflow as tf
import h5py
import numpy as np

from tensorflow.keras.utils import multi_gpu_model

import segmentation_models as sm





DATA_DIR = 'cloud_data'
BATCH_SIZE = 64

train_data = glob.glob(DATA_DIR + '/*/*')
random.shuffle(train_data)


def load_image(x):
    try:
        x = x.numpy().decode('utf-8')
    except:
        pass
        
    img = np.load('{}/image.npy'.format(x))
    mask = np.load('{}/mask.npy'.format(x))
            
    img = np.transpose(img, (1, 2, 0))
    mask = np.expand_dims(mask, axis=-1)
    
    bands = np.array([14, 25, 31])
    img = img[:, :, bands]
    
#     angular:37, 38, 39
#     doy: 42

    mask[mask > 0] = 1

    bands = bands.astype(np.float32)
    mask = mask.astype(np.float32)

    return img, mask





dataset = tf.compat.v1.data.Dataset.from_tensor_slices(train_data)
dataset = dataset.repeat()

dataset = dataset.map(lambda x : tf.py_function(
    load_image, [x], [tf.float32, tf.float32]), num_parallel_calls=30)

dataset = dataset.batch(BATCH_SIZE)
dataset = dataset.make_one_shot_iterator()
next_element = dataset.get_next()





def generate():
    while True:
        yield next_element





model = sm.Unet('resnet34', input_shape=(64, 64, 3), classes=1, encoder_weights='imagenet')


# model.fit(
#    dataset, epochs=500
# )

callbacks = [
#     tf.keras.callbacks.callbacks.ModelCheckpoint('unet_resnet34.weights.{epoch:02d}-{iou_score:.2f}.hdf5',
#                                                  monitor='iou_score', verbose=0, save_weights_only=True, 
#                                                  mode='max', period=1)
    tf.keras.callbacks.ReduceLROnPlateau(monitor='iou_score', factor=0.1, 
                                                patience=2, mode='max')
]

parallel_model = multi_gpu_model(model, gpus=4)

parallel_model.compile(
    tf.keras.optimizers.Adam(lr=0.001),
    loss=sm.losses.bce_jaccard_loss,
    metrics=[sm.metrics.iou_score, 'acc'],
)

parallel_model.fit_generator(generate(), steps_per_epoch=1000, epochs=15, callbacks=callbacks)

model.save('unet_resnet34.h5')




