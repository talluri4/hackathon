import os
import glob

import h5py
import numpy as np


DATA_DIR = '/home/hackathon/output_64_Javier_labelled'

train_data = glob.glob(DATA_DIR + '/*')


os.mkdir('cloud_data')

for x in train_data:
    try:
        hf_file = h5py.File(x, 'r')
        keys = list(hf_file.keys())
        
        subfolder = 'cloud_data/' + x.split('/')[-1]
        os.mkdir(subfolder)

        for k in keys:
            if hf_file[k + '/ClassificationAccuracy'][()] != 1:
                continue;
            img = hf_file[k + '/ImageFeatures'][()]
            mask = hf_file[k + '/ImageClassification'][()]

            os.mkdir(subfolder + '/{}'.format(k))
            np.save(subfolder + '/{}/{}'.format(k, 'image'), img)
            np.save(subfolder + '/{}/{}'.format(k, 'mask'), mask)
    except:
        print ('corrupt file')